using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupItem : MonoBehaviour
{

    private PlayerController playerController;

    void Start(){
        playerController = GameObject.Find("Player").GetComponent<PlayerController>();
    }

    void OnTriggerEnter2D(Collider2D other) 
    {
        // si el objeto que traspasó es el jugador
        if(other.gameObject.name == "Player") {
            playerController.HealthUp(5);
            Destroy(gameObject);
        }
    }
}
