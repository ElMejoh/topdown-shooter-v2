using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class RoundController : MonoBehaviour
{

    public GameObject[] roundText;
    public GameObject numberRound;
    public int round;
    public int countEnemieSpawn;
    public bool maxEnemieRound;
    private int timeRound;


    // Start is called before the first frame update
    void Start()
    {
        round = 1;
        print("Round: "+round);
        countEnemieSpawn = 50;
        numberRound.GetComponent<TextMeshProUGUI>().text = round.ToString();
        roundText[0].SetActive(true);
        roundText[1].SetActive(true);
        maxEnemieRound = false;
        timeRound = 180;
    }

    // Update is called once per frame
    void Update()
    {
        timeRound--;
        if(timeRound <= 0){
            roundText[0].SetActive(false);
            roundText[1].SetActive(false);
        }


        if(round == 1) {
            if (maxEnemieRound) NextRound(100);
        } else if(round == 2){
            if (maxEnemieRound) NextRound(100);
        } else if(round == 3){
            if (maxEnemieRound) NextRound(100);
        } else if(round == 4){
            if (maxEnemieRound) NextRound(200);
        } else if(round == 5){
            if (maxEnemieRound) NextRound(200);
        } else if(round == 6){
            if (maxEnemieRound) NextRound(200);
        } else if(round == 7){
            if (maxEnemieRound) NextRound(300);
        } else if(round == 8){
            if (maxEnemieRound) NextRound(300);
        } else if(round == 9){
            if (maxEnemieRound) NextRound(300);
        } else if(round == 10){
            if (maxEnemieRound) NextRound(400);
        }
    }

    public void NewEnemieRound(){
        countEnemieSpawn--;
        print("CountEnemySpawn: "+countEnemieSpawn);
    }

    void NextRound(int countEnem){
        if(GameObject.FindGameObjectsWithTag("Enemy").Length <= 0){
        round++;
        print("Round: "+round);
        maxEnemieRound = false;
        countEnemieSpawn = countEnem;
        numberRound.GetComponent<TextMeshProUGUI>().text = round.ToString();
        timeRound = 180;
        roundText[0].SetActive(true);
        roundText[1].SetActive(true);
        }
    }
}
