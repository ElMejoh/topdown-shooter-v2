using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

// TUTORIALES IMPORTANTES:
// https://www.youtube.com/watch?v=LNLVOjbrQj4

public class PlayerController : MonoBehaviour {
   
   // MOVEMENT + AIMING
    public float moveSpeed = 5f;
    public Rigidbody2D rbPlayer;
    public Camera camera;
    private Vector2 movement;
    private Vector2 mousePos;
    //-------------------------------------------------

    // SHOOTING
    public Transform firePoint;
    public GameObject bulletPrefab;
    public float bulletForce = 20f;

    //-------------------------------------------------

    // HEALTH
    public int maxHealth = 20;
    public int currentHealth;
    public HealthBar healthBar;

    //-------------------------------------------------

    // GAME OVER CONTROL
    private bool defeat = false;
    public GameObject textGameOver;
    public GameObject CountEnemy;

    //-------------------------------------------------

    // EXPERIENCIA XP
    public XPBar xPBar;
    private int levelPoints;
    private int maxXPValue;
    private int xpNum;
    private int level = 1;
    public GameObject levelText;
    
    
    

    
    void Start(){
        currentHealth = maxHealth;
        healthBar.SetMaxHealth(maxHealth);
        textGameOver.SetActive(false);

        levelPoints = 0;
        xpNum = 0;
        maxXPValue = 20;
        xPBar.SetMaxXP(maxXPValue);
    }

    void OnCollisionEnter2D(Collision2D coll){
        if (coll.gameObject.tag == "Enemy"){
            TakeDamage(1);
        }
    }

    void TakeDamage(int damage){
        currentHealth -= damage;
        healthBar.SetHealth(currentHealth);
        if (currentHealth <= 0){
            Destroy(gameObject);
            defeat = true;
            textGameOver.SetActive(true);
        }
    }

    void Update(){
        CountEnemy.GetComponent<TextMeshProUGUI>().text = GameObject.FindGameObjectsWithTag("Enemy").Length.ToString();    
        xPBar.SetXP(xpNum);
    }

    void FixedUpdate() {

        // Control de Movimiento
        PlayerMovement();  

        // Apuntar con el ratón
        ShootAiming();

        // Utilizar el prefab "Bullet" para disparar
        OpenFire();
    }

    void PlayerMovement() {

        // Coge los parametros del teclado
        movement.x = Input.GetAxisRaw("Horizontal");
        movement.y = Input.GetAxisRaw("Vertical");

        // Mueve al jugador segun su posicion actual, 
        // hacia donde indique el teclado, 
        // con la velocidad establecida
        rbPlayer.MovePosition(rbPlayer.position + movement * moveSpeed * Time.fixedDeltaTime);
    }

    void ShootAiming() {

        // Cogemos la posicion del raton en la pantalla
        mousePos = camera.ScreenToWorldPoint(Input.mousePosition);

        Vector2 lookDir = mousePos - rbPlayer.position;

        // Atan2 : Funcion matematica que devuelve el angulo entre los vectores
        //         x  y para devolver el angulo de apuntado
        float angle = Mathf.Atan2(lookDir.y, lookDir.x) * Mathf.Rad2Deg - 90f;
        rbPlayer.rotation = angle;
    }

    void OpenFire() {

        // Cuandro presionemos el Mouse1
        if (Input.GetButtonDown("Fire1")) {

            // Creamos la bala
            GameObject bullet = Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
            Rigidbody2D rbBullet = bullet.GetComponent<Rigidbody2D>();

            // La hacemos moverse a la velocidad establecida
            rbBullet.AddForce(firePoint.up * bulletForce, ForceMode2D.Impulse);

            
        }
    }

    public void EnemyKill(){
        xpNum++;

        if(xpNum >= maxXPValue){
            level++;
            levelPoints++;
            xpNum = 0;
            levelText.GetComponent<TextMeshProUGUI>().text = "Lv. "+level.ToString();  
            if(level == 2){
                maxXPValue = 30;
                xPBar.SetMaxXP(maxXPValue);  
            } else if (level == 3){
                maxXPValue = 50;
                xPBar.SetMaxXP(maxXPValue);
            } else if (level == 4){
                maxXPValue = 70;
                xPBar.SetMaxXP(maxXPValue);
            } else if (level == 5){
                maxXPValue = 100;
                xPBar.SetMaxXP(maxXPValue);
            } else if (level == 6){
                maxXPValue = 150;
                xPBar.SetMaxXP(maxXPValue);
            } else if (level == 7){
                maxXPValue = 200;
                xPBar.SetMaxXP(maxXPValue);
            } else if (level == 8){
                maxXPValue = 250;
                xPBar.SetMaxXP(maxXPValue);
            } else if (level == 9){
                maxXPValue = 300;
                xPBar.SetMaxXP(maxXPValue);
            } else if (level == 10){
                maxXPValue = 500;
                xPBar.SetMaxXP(maxXPValue);
            }
        }
    }

    public void HealthUp(int up){
        currentHealth+=up;
        if(currentHealth>maxHealth) {
            currentHealth = maxHealth;
        }
        healthBar.SetHealth(currentHealth);
    }

}
