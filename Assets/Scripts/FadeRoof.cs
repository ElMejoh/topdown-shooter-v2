using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeRoof : MonoBehaviour
{

    public SpriteRenderer roof;

    /*
    void OnCollisionEnter2D(Collision2D coll) {
        
        if (coll.gameObject.tag == "Player") {

            roof1.color = new Color(1f,1f,1f,0f);
            roof2.color = new Color(1f,1f,1f,0f);

            print("onCollision");
            
        }

    }
    */

    void OnTriggerEnter2D(Collider2D other) 
    {
        // si el objeto que traspasó es el jugador
        if(other.gameObject.name == "Player")
        {
            roof.color = new Color(1f,1f,1f,0.02f);

            print("onTriggerEnter");

        }
    }

    void OnTriggerExit2D(Collider2D other) {
        // si el objeto que traspasó es el jugador
        if(other.gameObject.name == "Player")
        {
            roof.color = new Color(0.1415094f, 0.1121501f, 0.03270737f, 1f);

            print("onTriggerExit");

        }
    }


}
