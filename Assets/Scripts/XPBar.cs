using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class XPBar : MonoBehaviour
{
    public Slider slider;

    public void SetMaxXP(int health){
        slider.maxValue = health;
        slider.value = 0;
    }

    public void SetXP(int health){
        slider.value = health;
    }
}
