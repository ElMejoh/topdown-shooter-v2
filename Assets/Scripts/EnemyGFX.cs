using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class EnemyGFX : MonoBehaviour
{

    private float xAxis;
    private float yAxis;

    public int Hitpoints;
    public int MaxHitPoints = 3;
    
    private GameObject player;
    private PlayerController playerController;

    public GameObject heartSpanw;

    void Start(){
        xAxis = transform.localScale.x;
        yAxis = transform.localScale.y;
        Hitpoints = MaxHitPoints;

        player = GameObject.Find("Player");
        playerController = player.GetComponent<PlayerController>();

    }

    void OnCollisionEnter2D(Collision2D coll){
        if (coll.gameObject.tag == "Bullet"){
            TakeDamage(1);
        }

        if(Hitpoints <= 0) {
            Destroy(gameObject);
            playerController.EnemyKill();

            float i = Random.Range(0, 9);
            if(i==1){
                Instantiate(heartSpanw, transform.position, Quaternion.identity);
            }
            
        }
    }

    void TakeDamage(int damage){
        Hitpoints -= damage;
    }

    // Update is called once per frame
    void Update()
    {
        if(GetComponent<AIPath>().desiredVelocity.x >= 0.01f){
            
            transform.localScale = new Vector3(xAxis, yAxis, 1f);
        
        } else if (GetComponent<AIPath>().desiredVelocity.x <= -0.01f){
            
            transform.localScale = new Vector3(-xAxis, yAxis, 1f);
        
        }
        
    }
}
