using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class SpawnEnemy : MonoBehaviour
{

    public GameObject[] enemy;

    public float interval = 10;

    public int maxEnemies = 50;

    public RoundController roundController;

    // Start is called before the first frame update
    void Start() {
        InvokeRepeating("SpawnNext", interval, interval);
    }

    void SpawnNext(){
        
        if(roundController.round == 1) {
            SpawnEnemyDesigned(6, 9);  
        } else if(roundController.round == 2){
            SpawnEnemyDesigned(0, 4);
        } else if(roundController.round == 3){
            SpawnEnemyDesigned(0, 5);
        } else if(roundController.round == 4){
            SpawnEnemyDesigned(0, 5);
        } else if(roundController.round == 5){
            SpawnEnemyDesigned(0, 6);
        } else if(roundController.round == 6){
            SpawnEnemyDesigned(3, 7);
        } else if(roundController.round == 7){
            SpawnEnemyDesigned(4, 8);
        } else if(roundController.round == 8){
            SpawnEnemyDesigned(4, 8);
        } else if(roundController.round == 9){
            SpawnEnemyDesigned(5, 9);
        } else if(roundController.round == 10){
            SpawnEnemyDesigned(6, 9);
        }
        
    }

    void SpawnEnemyDesigned(int idEnMin, int idEnMax){
        
        float i = Random.Range(0, 4);
        if (i==1 && GameObject.FindGameObjectsWithTag("Enemy").Length < maxEnemies && !roundController.maxEnemieRound){
            Instantiate(enemy[Random.Range(idEnMin, idEnMax)], transform.position, Quaternion.identity);
            roundController.NewEnemieRound();
            if (roundController.countEnemieSpawn <= 0){
                roundController.maxEnemieRound = true;
            }
        }

    }

    


}
